<?php


    //メニューバー表示
add_theme_support('menus');

register_nav_menus(
    array(
        'place_global'  => 'グローバル'
    )
);

//ウィジェットエリア表示
register_sidebar(
    array(
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="right-card-third-contents__title">',
        'after_title' => '</h3>',

    )
);
//js読み込み


add_action('wp_enqueue_scripts','twpp_enqueue_scripts');

//アイキャッチ画像
add_theme_support( 'post-thumbnails' );

//thumbnailの大きさ指定をはずす
add_filter( 'post_thumbnail_html', 'custom_attribute' );
function custom_attribute( $html ){
    // width height を削除する
    $html = preg_replace('/(width|height)="\d*"\s/', '', $html);
    return $html;
}

//menuのaタグのクラス名指定
add_filter('walker_nav_menu_start_el','add_class_on_link',10,4);
    function add_class_on_link($item_output,$item) {
        return preg_replace('/(<a.*?)/','$1' . " class='menu__item'", $item_output);
    }


//ページネーション
function pagenation($pages = '', $range = 2){
    $showitems = ($range * 1)+1;
    global $paged;
    if(empty($paged)) $paged = 1;
    if($pages == ''){
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages){
            $pages = 1;
        }
    }
    if(1 != $pages){
        echo "<div class=\"content-left__page-tag\">";

        echo "<ol class=\"page-tag\">\n";
        for ($i=1; $i <= $pages; $i++){
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
                echo ($paged == $i)? "<li class=\"page-tag__item page-tag__item--active\"></li>": // 現在のページの数字はリンク無し
                    "<li><a class=\"page-tag__item\" href='".get_pagenum_link($i)."'></a></li>";
            }
        }
        echo "</ol>\n";
        echo "</div>\n";
    }
}



//最近の投稿　ウィジェット  参考:https://lblevery.com/sfn/create/wordpress-tips/wordpress-theming/make-your-own-widgets/
class imgNewPostWidget extends WP_Widget {
    function imgNewPostWidget() {
        parent::WP_Widget(
            false,
            $name = '画像付き最近の投稿',
            array('description' => '最近の投稿を画像付きで表示します。',)

        );
    }

    function form( $instance ) {
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('表示する投稿数:'); ?></label>
            <input type="text" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" value="<?php echo esc_attr( $instance['limit'] ); ?>" size="3">
        </p>
        <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['limit'] = is_numeric($new_instance['limit']) ? $new_instance['limit'] : 5;
        return $instance;
    }

    function widget( $args, $instance) {
        extract( $args );

        ?>
        <div class="right-card-third">
            <div class="right-card-third-contents">
                <?php if($instance['title'] != ''){
                $title = apply_filters('widget_title', $instance['title']);
                }
                if ( $title ) {
                echo $before_title . $title . $after_title;
                }
                ?>
                <?php
                query_posts("posts_pre_page=".$instance['limit']);
                if(have_posts()) :
                    while(have_posts()): the_post();
                        ?>

                        <article>
                            <a href="<php the_permalink(); ?>" class="right-card-third-link">
                                <?php if(has_post_thumbnail() ): ?>
                                    <?php the_post_thumbnail('thumbnail',array('class'=>'right-card-third-link__img'));  ?>
                                <?php else : ?>
                                    <img src="<?php get_template_directory_uri(); ?>/iimg/noimg.gif"class="right-card-third-link__img">
                                <?php endif; ?>
                                <p class="right-card-third-link__title"><?php the_title(); ?></p>
                            </a>
                        </article>
                    <?php endwhile; endif; ?>

            </div>
        </div>
        <?php
    }


}
register_widget('imgNewPostWidget');

// 人気記事出力用関数
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
// 記事viewカウント用関数
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);



