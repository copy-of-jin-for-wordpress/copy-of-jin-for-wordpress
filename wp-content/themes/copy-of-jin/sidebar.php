<aside class="content-right">
    <? dynamic_sidebar(); ?>
    <div class="right-card-first">
        <article>
            <h3 class="right-card-first__title">WordPressテーマ「JIN」</h3>
            <div class="right-card-first-contents">
                <div><img src="<?php echo get_template_directory_uri(); ?>/img/right-card-first-contents__img.png" alt="right-card-first-img" class="right-card-first-contents__img"></div>
                <p class="right-card-first-contents__lead">「アフィリエイト」と「SEO」に必要な機能を全て詰め込んだテーマが完成。売れるサイト作りを確実に後押しします。</p>
                <a href="#" class="right-card-first-contents__btn">今すぐダウンロード</a>
            </div>
        </article>
    </div>
    <div class="right-card-second">
        <article>
            <div class="right-card-second-text">
                <a class="right-card-second-text__user-icon" href="#">
                    <img class="user-icon-image" src="<?php echo get_field('user-icon','user_1'); ?>" alt="right-card-second__user-icon">
                </a>
                <span class="right-card-second-text__user-name">
                    <?php $user = wp_get_current_user();
                    echo $user->user_login;
                    ?>
                </span>
                <span class="right-card-second-text__user-name-sub"><?php the_author_meta('sub-title',1); ?></span>
                <p class="right-card-second-text__user-info"><?php the_author_meta('user-info',1); ?></p>
                <a href="#" class="right-card-second-text__btn">詳しく見る</a>
            </div>
            <div class="user-SNS">
                <span class="user-SNS__title">＼ Follow me ／</span>
                <a href="<?php the_author_meta('twitter-url',1); ?>" class="user-SNS__icon"><span class="fab fa-twitter SNS-icon-img"></span></a>
                <a href="<?php the_author_meta('facebook-url',1); ?>" class="user-SNS__icon"><span class="fab fa-facebook-square SNS-icon-img"></span></a>
                <a href="<?php the_author_meta('instagram-url',1); ?>" class="user-SNS__icon"><span class="fab fa-instagram SNS-icon-img"></span></a>
                <a href="<?php the_author_meta('line-url',1); ?>" class="user-SNS__icon"><span class="fab fa-line SNS-icon-img"></span></a>
            </div>
        </article>
    </div>
    <div class="right-card-third">
            <div class="right-card-third-contents">
                <h3 class="right-card-third-contents__title">RECENT POST</h3>
                <?php $args = array(
                        'post_per_page' => 5,
                        'orderby' => 'date'
                );
                $posts = get_posts($args);
                foreach($posts as $post):
                setup_postdata($post);
                ?>
                <article>
                    <a href="<?php the_permalink(); ?>" class="right-card-third-link">
                        <?php if(has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('thumbnail',array('class'=>'right-card-third-link__img')); ?>
                        <?php else : ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/card-image-first.jpg" alt="card-image-first" class="right-card-third-link__img">
                        <?php endif; ?>
                        <p class="right-card-third-link__title"><?php the_title(); ?></p>
                    </a>
                </article>
                <?php
                endforeach;
                wp_reset_postdata();
                ?>
            </div>
    </div>
    <div class="right-card-fourth">
        <div class="right-card-fourth-contents">
            <h3 class="right-card-fourth-contents__title">CATEGORY</h3>
            <form>
                <?php $categories = get_categories(array('taxonomy' => 'category')); if($categories) : ?>

                <select class="right-card-fourth-contents__category-select" name="category" onChange="location.href=value;" size="1">
                    <option value="" selected="selected">カテゴリーを選択</option>
                    <? foreach ($categories as $category): ?>
                    <option value="<?php echo esc_html(get_category_link($category->term_id)); ?>"><?php echo esc_html($category->name); ?></option>
                    <?php endforeach; ?>
                </select>
                <? endif; ?>
            </form>
        </div>
    </div>
    <div class="right-card-fifth">
        <article>
            <div class="right-card-fifth-contents">
                <h3 class="right-card-fifth-contents__title">TAG</h3>
                <ul class="article-tag-list">
                    <?php
                    $term_list = get_terms('post_tag');
                    $result_list = [];
                    foreach ($term_list as $term) {
                        $u = (get_term_link($term,'post_tag'));
                        echo "<li><a class=\"article-tag-list__item\" href='".$u."'>"."<i class=\"fas fa-tag tag\"></i>".$term->name."</a></li>";
                    }
                    ?>
                </ul>
            </div>
        </article>
    </div>
    <div class="right-card-sixth">
        <article>
            <div class="right-card-sixth-contents">
                <h3 class="right-card-sixth-contents__title">SEARCH</h3>
                <?php get_search_form(); ?>
            </div>
        </article>
    </div>
    <div class="right-card-seventh">
        <article>
            <div class="right-card-seventh-contents">
                <h3 class="right-card-seventh-contents__title">RECOMMEND</h3>
                <?php
                $cat = get_the_category();
                $cat_name = $cat[0]->cat_name;
                $cat_id = $cat[0]->cat_ID;
                $args = array(
                        'orderby'=>'rand',
                        'category'=>$cat_id,
                        'post_per_page'=>5,
                );
                $post = get_posts($args);
                if ($post) :
                    setup_postdata($post);
                ?>
                <a href="<?php the_permalink(); ?>" class="recommend-article">
                    <?php if(has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail('thumbnail',array('class'=>'recommend-article__img')); ?>
                    <?php else : ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/noimg.gif" alt="recommend-image" class="recommend-article__img">
                    <?php endif; ?>
                    <p class="recommend-article__title"><?php the_title(); ?></p>
                </a>
                <?php
                    endif;
                    wp_reset_postdata();
                ?>
            </div>
        </article>
    </div>
</aside>