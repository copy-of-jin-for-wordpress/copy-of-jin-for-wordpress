<?php get_header(); ?>
<div class="contents">
    <div class="content-left">
        <div class="article-head">
            <?php
            $cat = get_the_category();
            $cat_name = $cat[0]->cat_name;
            $cat_id = $cat[0]->cat_ID;
            $cat_url = get_category_link($cat_id);
            ?>
            <a href="<?php echo $cat_url; ?>" class="category-icon">
                <?php echo $cat_name; ?>
            </a>
            <h2 class="article-head__title"><?php the_title(); ?></h2>
            <time class="article-head__date"><i class="far fa-clock"></i> <?php echo get_the_date(); ?></time>
            <?php if(has_post_thumbnail()) :?>
                <?php the_post_thumbnail('thumbnail', array('class'=>'article-head__img')) ?>
            <?php else :?>
            <div><img src="<?php echo get_template_directory_uri(); ?>/img/noimg.gif" alt="card-image-second" class="article-head__img"></div>
            <?php
            endif;
            wp_reset_postdata();
            ?>
            <div class="SNS-share-btn">
                <a class="SNS-share-btn__twitter" href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>"><i class="fab fa-twitter"></i></a>
                <a class="SNS-share-btn__facebook" href="https://www.facebook.com/share.php?u=<?php the_permalink(); ?>"><i class="fab fa-facebook-f"></i></a>
                <a class="SNS-share-btn__bookmark" href="https://b.hatena.ne.jp/add?mode=confirm&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>">B!</a>
                <a class="SNS-share-btn__pocket" href="https://getpocket.com/edit?url=<?php the_permalink(); ?>"><i class="fab fa-get-pocket"></i></a>
                <a class="SNS-share-btn__line" href="https://social-plugins.line.me/lineit/share?url=<?php the_permalink(); ?>"><i class="fab fa-line"></i></a>
            </div>

            <div class="article-contents">
                <?php the_content(); ?>

            </div>
            <div class="download-box">
                <div class="download-box-container">
                    <h3 class="download-box-container__title">WordPressテーマ「JIN」</h3>
                    <img class="download-box-container__img" src="<?php echo get_template_directory_uri(); ?>/img/download-img.png" alt="download-image">
                    <p class="download-box-container__paragraph">JIN（ジン）は読む人も書く人も、どちらも心地よく使える最高のWordPressテーマを目指して作り上げました。</p>
                    <p class="download-box-container__paragraph">月100万円以上稼ぐアフィリエイターのノウハウが凝縮されているテーマです。売れるメディア作りに必要なものは、このテーマ内にすべて揃えました。</p>
                    <a href="#" class="download-box-container__btn">ダウンロード</a>
                </div>
            </div>
        </div>
    </div>
    <?php get_sidebar(); ?>

    <aside class="article-lists">
        <div class="related-post-list">
            <h2 class="related-post-list__title"><i class="fas fa-file-alt related-post-logo"></i>RELATED POST</h2>
            <div class="related-post-list-cards">
                <?php
                    $arg = array(
                            'post_per_page'=>3,
                            'orderby'=>'date',
                            'category'=> $cat_id,
                    );
                    $posts = get_posts($arg);
                    if($posts) :
                        foreach($posts as $post) :
                            setup_postdata($post);
                ?>
                <a href="<?php the_permalink(); ?>" class="related-post-card">
                    <article>
                        <span class="related-post-card__icon"><?php echo $cat_name ?></span>
                        <?php if(has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('thumbnail',array('class'=>'related-post-card__img')); ?>
                        <? else : ?>
                        <div><img src="<?php echo get_template_directory_uri(); ?>/img/related-post-first.jpg" alt="related-post-first-img" class="related-post-card__img"></div>
                        <? endif ?>
                        <h4 class="related-post-card__title"><?php the_title(); ?></h4>
                        <time class="related-post-card__date"><?php echo get_the_date(); ?></time>
                    </article>
                </a>
                <?php
                    endforeach;
                    endif;
                wp_reset_postdata();

                ?>
            </div>
        </div>
        <div class="recent-post">
            <?php
            $previous_post = get_previous_post();
            if(!empty($previous_post)):
                $prev_thum = get_the_post_thumbnail($previous_post->ID,array('class'=>' recent-post-prev__img'));
            ?>
            <a class="recent-post-prev" href="<?php echo get_permalink($previous_post->ID); ?>">
                <span class="recent-post-prev__icon">PREV</span>
                <?php if(!empty($prev_thum)) :?>
                <?php echo $prev_thum; ?>
                <?php else : ?>
                <img src="<? echo get_template_directory_uri(); ?>/img/noimg.gif" class="recent-post-prev__img">
                <? endif; ?>
                <h5 class="recent-post-prev__title"><?php echo $previous_post->post_title; ?></h5>
            </a>
            <?php else :?>
            <div class = "recent-post-prev"></div>
            <?php endif; ?>

            <?php
            $next_post = get_next_post();
            if(!empty($next_post)):
                $next_thum = get_the_post_thumbnail($next_post->ID,array('class'=>' recent-post-next__img'));
                ?>
                <a class="recent-post-next" href="<?php echo get_permalink($next_post->ID); ?>">
                    <span class="recent-post-next__icon">NEXT</span>
                    <?php if(!empty($next_thum)) :?>
                        <?php echo $next_thum; ?>
                    <?php else : ?>
                        <img src="<? echo get_template_directory_uri(); ?>/img/noimg.gif" class="recent-post-next__img">
                    <? endif; ?>
                    <h5 class="recent-post-next__title"><?php echo $next_post->post_title; ?></h5>
                </a>
            <?php else :?>
                <div class = "recent-post-next"></div>
            <?php endif; ?>
        </div>

    </aside>


    <aside class="breadcrumbs-container-md">
        <ul class="breadcrumbs-list">
            <li><a href="<?php echo home_url(); ?>" class="breadcrumbs-list__item"><i class="fas fa-home breadcrumbs-home"></i>HOME</a></li>
            <li><i class="breadcrumbs-list__item fas fa-chevron-right"></i></li>
            <li><a href="<?php echo get_category_link($cat_id); ?>" class="breadcrumbs-list__item"><?php echo $cat_name ?></a></li>
            <li><i class="breadcrumbs-list__item fas fa-chevron-right"></i></li>
            <li class="breadcrumbs-list__item"><?php the_title(); ?></li>
        </ul>
    </aside>
</div>

<aside class="breadcrumbs-container-lg">
    <ul class="breadcrumbs-list">
        <li><a href="<?php echo home_url(); ?>" class="breadcrumbs-list__item"><i class="fas fa-home breadcrumbs-home"></i>HOME</a></li>
        <li><i class="breadcrumbs-list__item fas fa-chevron-right"></i></li>
        <li><a href="<?php echo get_category_link($cat_id); ?>" class="breadcrumbs-list__item"><?php echo $cat_name ?></a></li>
        <li><i class="breadcrumbs-list__item fas fa-chevron-right"></i></li>
        <li class="breadcrumbs-list__item"><?php the_title(); ?></li>
    </ul>
</aside>

<?php get_footer(); ?>