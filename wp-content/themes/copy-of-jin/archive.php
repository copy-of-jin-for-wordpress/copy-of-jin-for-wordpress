<?php get_header(); ?>
<?php $cat = get_the_category();
$cat_name = $cat[0]->cat_name;
?>
    <div class="contents">
        <div class="content-left">
            <div class="content-left__title">
                <p class="category-title-en">― ARCHIVES ―</p>
                <h2 class="category-title-ja">
                    <?php echo get_the_date('Y年　m月'); ?>
                </h2>
            </div>
            <?php if(have_posts()): while(have_posts()) : the_post(); ?>
                <div class="content-left__card">
                    <article>
                        <a class="card-left" href="<?php the_permalink(); ?>">
                            <p class="card-left__icon"><?php echo $cat_name; ?></p>
                            <?php if(has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('thumbnail',array('class'=>'card-left__img')); ?>
                            <?php else : ?>
                                <img class="card-left__img" src="<?php echo get_template_directory_uri(); ?>/img/noimg.gif" alt="card-image-seventh">
                            <?php endif; ?>
                            <div class="left-text">
                                <h4 class="left-text__title"><?php the_title(); ?></h4>
                                <time class="left-text__date"><?php echo get_the_date(); ?></time>
                            </div>
                        </a>
                    </article>
                </div>
            <?php endwhile; endif; ?>



        </div>
        <?php get_sidebar(); ?>
        <aside class="breadcrumbs-container-md">
            <ul class="breadcrumbs-list">
                <li><a href="<?php echo home_url(); ?>" class="breadcrumbs-list__item"><i class="fas fa-home breadcrumbs-home"></i>HOME</a></li>
                <li><i class="breadcrumbs-list__item fas fa-chevron-right"></i></li>
                <li><a href="<?php echo get_month_link(false,false); ?>" class="breadcrumbs-list__item"><?php echo get_the_date('Y年　m月') ?></a></li>
            </ul>
        </aside>
    </div>

    <aside class="breadcrumbs-container-lg">
        <ul class="breadcrumbs-list">
            <li><a href="<?php echo home_url(); ?>" class="breadcrumbs-list__item"><i class="fas fa-home breadcrumbs-home"></i>HOME</a></li>
            <li><i class="breadcrumbs-list__item fas fa-chevron-right"></i></li>
            <li><a href="<?php echo get_month_link(false,false); ?>" class="breadcrumbs-list__item"><?php echo get_the_date('Y年　m月') ?></a></li>
        </ul>
    </aside>
<?php get_footer(); ?>