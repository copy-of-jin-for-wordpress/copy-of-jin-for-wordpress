<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <?php wp_head(); ?>
</head>
<body>
<header class="header">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header__logo"><span class="header__logo--green">JIN </span> Demo Site 4</a>
    <i class="fas fa-bars header__menu-bar"></i>
    <?php wp_nav_menu(); ?>
</header>