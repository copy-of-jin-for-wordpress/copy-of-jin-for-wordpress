<?php get_header(); ?>
    <div class="contents">
        <div class="content-left">
            <?php if(have_posts()): while(have_posts()): the_post(); ?>

                    <div class="content-left__card">
                        <article>
                            <a class="card-left" href="<? the_permalink();?>">
                                <?php  $cat = get_the_category(); $cat = $cat[0]; {
                                    echo '<span class="card-left__icon ' . $cat->category_nicename .'">';
                                    } ?>
                                <?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></span>

                                <?php if(has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('thumbnail',array('class'=>'card-left__img')); ?>
                                <?php else : ?>
                                    <div><img class="card-left__img" src="<?php echo get_template_directory_uri(); ?>/img/noimg.gif"></div>
                                <?php endif; ?>
                                <div class="left-text">
                                    <h4 class="left-text__title"><?php the_title(); ?></h4>
                                    <time class="left-text__date"><?php echo get_the_date('m月 d,Y'); ?></time>
                                </div>
                            </a>
                        </article>
                    </div>
            <?php endwhile; endif; ?>
            <?php
            if(function_exists('pagenation')) {
                pagenation();
            }
            ?>
        </div>
        <?php get_sidebar(); ?>
    </div>
    <?php get_footer(); ?>
