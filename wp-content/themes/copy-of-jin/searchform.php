<form class="search-box" action="<?php echo get_home_url(); ?>" method="get">
    <input class="search-box__text-box" name="s" type="text"/>
    <button class="search-box__btn" type="submit"><span class="fas fa-search"></span></button>
</form>
