<footer class="footer">
    <div class="footer-contents">
        <div class="footer-first">
            <div class="footer-first-contents">
                <div class="footer-first-main">
                    <a class="footer-first-main__logo" href="#"><img class="footer-logo-img" src="<?php echo get_template_directory_uri(); ?>/img/footer-logo.png" alt="footer-logo"></a>
                    <p class="footer-first-main__lead">読む人も、書く人も、すべてを考え抜いたデザインを実現。真の使いやすさを追求したWordPressテーマです。</p>
                    <p class="footer-first-main__lead">「アフィリエイト」と「SEO」に必要なものは、すべて詰め込みました。売れるサイト作りを確実に後押しします。</p>
                    <a class="footer-first-main__btn" href="#"><i class="fas fa-chevron-circle-right footer-btn-icon"></i>今すぐダウンロード</a>
                </div>
                <div class="footer-article-tag">
                    <h3 class="footer-article-tag__title">TAG</h3>
                    <ul class="footer-article-tag-list">
                        <?php
                        $term_list = get_terms('post_tag');
                        $result_list = [];
                        foreach ($term_list as $term) :
                            $u = (get_term_link($term));
                            ?>
                            <li><a class="footer-article-tag-list__item" href="<?php echo $u ?>"><i class="fas fa-tag tag"></i><?php echo $term->name ?></a></li>
                        <?php endforeach; ?>
                    </ul>

                </div>
            </div>

        </div>
        <div class="footer-lists">
            <div class="footer-second">
                <div class="footer-second-contents">
                    <h3 class="footer-second-contents__title">CATEGORY</h3>
                    <ul class="category-list">
                        <?php
                        $args = array(
                            'orderby' => 'term_order',
                        );
                        $categories = get_categories($args);
                        ?>
                        <?php foreach($categories as $cat) :?>
                        <li>
                            <a class="category-list__item" href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name; ?> </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

            </div>
            <div class="footer-third">
                <div class="footer-third-contents">
                    <h3 class="footer-third-contents__title">ARCHIVES</h3>
                    <ul class="archives-list">
                        <li>
                            <?php

                            $cat_slug = '取得したいカテゴリー';
                            $args = array(
                                'posts_per_page' => -1
                            );


                            //ここからはget_posts()でも
                            $archive_query = new WP_Query($args);

                            while ($archive_query->have_posts()) {
                                $archive_query->the_post();
                                //年月毎に記事情報を配列に格納
                                $archive_list[get_the_time('Y/n', $post->ID)][] = $post->post_title;
                            }
                            wp_reset_postdata();

                            ?>
                            <?php
                            if($archive_list) : ?>
                            <?php foreach( $archive_list as $year_month => $archive ) :
                            $year_month_arr = explode( '/', $year_month );
                            ?>
                        <li>
                            <a class="archives-list__item" href="<?php echo get_month_link(false,false); ?>">
                                <?php echo $year_month_arr[0].'年'.$year_month_arr[1].'月' ?> [<?php echo count( $archive ) ?>]</a>
                        </li>
                        <?php
                        endforeach;
                        endif;
                        ?>


                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="footer-fourth">
            <div class="footer-fourth-contents">
                <h3 class="footer-fourth-contents__title">POPULAR POST</h3>
                <div class="popular-post-list">
                    <?php
                    setPostViews(get_the_ID());


                    $args = array(
                        'meta_key' => 'post_views_count',
                        'orderby' => 'meta_value_num',
                        'order' => 'DESC',
                        'posts_per_page' => 3
                    );
                    $i = 0;
                    $query = new WP_Query($args);
                    if ($query->have_posts()) :
                        while ($query->have_posts()) :
                            $i = $i +1;
                            $query->the_post();
                            ?>

                    <article>
                        <a class="popular-post" href="<?php the_permalink(); ?>">
                            <span class="popular-post__icon"><?php echo $i ?></span>
                            <?php if(has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('thumbnail',array('class'=>'popular-post__img')); ?>
                            <?php else : ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/noimg.gif" alt="card-image-first" class="popular-post__img">
                            <? endif; ?>
                            <p class="popular-post__title"><?php the_title(); ?></p>
                        </a>
                    </article>
                    <?php
                        endwhile;
                        endif;
                        wp_reset_postdata();
                    ?>
                </div>
            </div>

        </div>
    </div>
    <span class="copy-right"><i class="far fa-copyright"></i>2017–2020  JIN Demo Site 4</span>
</footer>
<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri()?>/dist/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri().'/style/dist/main.js?ver='.date('YmdGis', filemtime(get_template_directory().'/style/dist/main.js')); ?>"></script>


</body>

</html>