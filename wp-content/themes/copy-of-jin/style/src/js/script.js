$(".related-post-list-cards").slick({
    infinite: true,
    autoplay: true,
    dots: true,
    prevArrow: '<button class = "related-post-list__btn--left related-post-list__btn"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button class = "related-post-list__btn--right related-post-list__btn"><i class="fas fa-chevron-right"></i></button>',
    slidesToShow: 3,
    responsive: [{
        breakpoint: 767,
        settings: {
            slidesToShow: 1,
            centerPadding: "25%",
            centerMode: true
        }
    }]
})